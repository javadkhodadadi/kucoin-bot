<?php
ini_set('display_errors', 1);
require __DIR__ . '/vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

use KuCoin\Futures\SDK\Auth;
use KuCoin\SDK\Exceptions\HttpException;
use KuCoin\SDK\Exceptions\BusinessException;
use KuCoin\Futures\SDK\KuCoinFuturesApi;
use KuCoin\Futures\SDK\PrivateApi\Order;
use KuCoin\Futures\SDK\PublicApi\Symbol;
use KuCoin\Futures\SDK\PrivateApi\Position;

function createOrder($getTicker,string $symbol,string $side,$isClose = false){
  //https://api-futures.kucoin.com/api/v1/contracts/active
  $validCurrency=array(
    "COMPUSDTM"   =>array("multiplier"=>0.01,"round"=>0),
    "DASHUSDTM"   =>array("multiplier"=>0.01,"round"=>0),
    "ETCUSDTM"    =>array("multiplier"=>0.1,"round"=>0),
    "GRTUSDTM"    =>array("multiplier"=>1.0,"round"=>0),
    "HBARUSDTM"   =>array("multiplier"=>10.0,"round"=>0),
    "MATICUSDTM"  =>array("multiplier"=>10.0,"round"=>0),
    "ONTUSDTM"    =>array("multiplier"=>1.0,"round"=>0),
    "SXPUSDTM"    =>array("multiplier"=>1.0,"round"=>0),
    "XBTUSDTM"    =>array("multiplier"=>0.001,"round"=>0),
  );
  $lossLimitPercent= (float) $_ENV["lossLimitPercent"];
  $profitLimitPercent = (float) $_ENV["profitLimitPercent"];

  $auth = new Auth($_ENV["key"], $_ENV["secret"], $_ENV["pass"], Auth::API_KEY_VERSION_V2);
  // $symbolClass = new Symbol();
  $leverage = $_ENV["leverage"];
  $amount = $_ENV["maxAmountPerOrder"];
  // $getTicker = $symbolClass->getTicker($symbol);
  $price = $getTicker["bestAskPrice"];

  if($side=="sell"){
    $price = $getTicker["bestBidPrice"];
  }
  $multiplier = $validCurrency[$symbol]["multiplier"];
  $size = floor(($amount / $price)/$multiplier);
  if($size>0){
    $closeApi = new Order($auth);
    
    $order = [
      'clientOid' => uniqid(),
      'price'     => $price,
      'leverage'  => $leverage,
      'size'      => $size,
      'visibleSize' => $size,
      'symbol'    => $symbol,
      'type'      => 'market',
      'side'      => $side,
      'remark'    => 'BOT',
      'closeOrder'=> $isClose,
    ];
    // var_dump("side: $side | symbol: $symbol | size: $size | price: $price");
    try {
      $result = $closeApi->create($order);
      var_dump($symbol,$side,$result);
    } catch (\Throwable $e) {
      $result=array();
      var_dump($e->getMessage());
    }
  }else{
    $result =array();
    var_dump("$symbol: not valid size. size is 0");
  }
  return $result;
}
function orderProfitLoss($getTicker,string $symbol,string $side,$isClose = true){
  //https://api-futures.kucoin.com/api/v1/contracts/active
  $validCurrency=array(
    "COMPUSDTM"   =>array("multiplier"=>0.01,"round"=>0),
    "DASHUSDTM"   =>array("multiplier"=>0.01,"round"=>0),
    "ETCUSDTM"    =>array("multiplier"=>0.1,"round"=>0),
    "GRTUSDTM"    =>array("multiplier"=>1.0,"round"=>0),
    "HBARUSDTM"   =>array("multiplier"=>10.0,"round"=>0),
    "MATICUSDTM"  =>array("multiplier"=>10.0,"round"=>0),
    "ONTUSDTM"    =>array("multiplier"=>1.0,"round"=>0),
    "SXPUSDTM"    =>array("multiplier"=>1.0,"round"=>0),
    "XBTUSDTM"    =>array("multiplier"=>0.001,"round"=>0),
  );
  $lossLimitPercent= (float) $_ENV["lossLimitPercent"];
  $profitLimitPercent = (float) $_ENV["profitLimitPercent"];

  $auth = new Auth($_ENV["key"], $_ENV["secret"], $_ENV["pass"], Auth::API_KEY_VERSION_V2);
  // $symbolClass = new Symbol();
  $leverage = $_ENV["leverage"];
  $amount = $_ENV["maxAmountPerOrder"];
  // $getTicker = $symbolClass->getTicker($symbol);
  $price = $getTicker["bestAskPrice"];

  $stopPriceLoss = $price+(($price/100)*$lossLimitPercent);
  $stopPriceProfit = $price-(($price/100)*$profitLimitPercent);
  $stopLoss = "up";
  $stopProfit = "down";
  if($side=="sell"){
    $price = $getTicker["bestBidPrice"];
    $stopPriceLoss = $price-(($price/100)*$lossLimitPercent);
    $stopPriceProfit = $price+(($price/100)*$profitLimitPercent);
    $stopLoss = "down";
    $stopProfit = "up";
  }
  $multiplier = $validCurrency[$symbol]["multiplier"];
  $size = floor(($amount / $price)/$multiplier);
  if($size>0){
    $closeApi = new Order($auth);
    
    $orderLoss = [
      'clientOid' => uniqid(),
      'stopPriceType' => "MP",
      'stopPrice'=> $stopPriceLoss,
      'price'     => $price,
      'size'      => $size,
      'stop'=> $stopLoss,
      'leverage'  => $leverage,
      'type'      => 'market',
      'side'      => $side,
      'symbol'    => $symbol,
      'remark'    => 'BOT',
      'closeOrder'=> $isClose,
    ];

    $orderProfit = [
      'clientOid' => uniqid(),
      'stopPriceType' => "MP",
      'stopPrice'=> $stopPriceProfit,
      'price'     => $price,
      'size'      => round($size/2),
      'stop'=> $stopProfit,
      'leverage'  => $leverage,
      'type'      => 'market',
      'side'      => $side,
      'symbol'    => $symbol,
      'remark'    => 'BOT',
      'closeOrder'=> $isClose,
    ];
    // var_dump("side: $side | symbol: $symbol | size: $size | price: $price");
    try {
      $result = $closeApi->create($orderLoss);
      $closeApi->create($orderProfit);
      var_dump($symbol,$side,$result);
    } catch (\Throwable $e) {
      $result=array();
      var_dump($e->getMessage());
    }
  }else{
    $result =array();
    var_dump("$symbol: not valid size. size is 0");
  }
  return $result;
}
function cancelOrder($orderId){
  $auth = new Auth($_ENV["key"], $_ENV["secret"], $_ENV["pass"], Auth::API_KEY_VERSION_V2);
  $api = new Order($auth);
  var_dump($orderId);
  try {
    $result = $api->cancel($orderId);
  } catch (\Throwable $e) {
    $result =array();
  }
  return $result;
}
function batchCancel($symbol){
  $auth = new Auth($_ENV["key"], $_ENV["secret"], $_ENV["pass"], Auth::API_KEY_VERSION_V2);
  $api = new Order($auth);
  $order = [
    'symbol'    => $symbol,
  ];
  try {
    $result = $api->batchCancel($symbol);
  } catch (\Throwable $e) {
    $result =array();
    var_dump($e->getMessage());
  }
  return $result;
}
function orderList($status){
  $auth = new Auth($_ENV["key"], $_ENV["secret"], $_ENV["pass"], Auth::API_KEY_VERSION_V2);
  $api = new Order($auth);
  $order = [
    "status"=>$status
  ];
  try {
    $result = $api->getList($order);
  } catch (\Throwable $e) {
    $result = array();
  }
  return $result;
}

function orderProfitLossCancel($symbol){
  $auth = new Auth($_ENV["key"], $_ENV["secret"], $_ENV["pass"], Auth::API_KEY_VERSION_V2);
  $order = new Order($auth);
  $result= $order->stopOrders($symbol);
  var_dump($result);
}

function positionDetails(string $symbol){
  try {
    $auth = new Auth($_ENV["key"], $_ENV["secret"], $_ENV["pass"], Auth::API_KEY_VERSION_V2);
    $api = new Position($auth);
    $details = $api->getDetail($symbol);
    
    if(isset($details["avgEntryPrice"])){
      if($details["avgEntryPrice"]>$details["liquidationPrice"]){
        $details["side"]="buy";
      }else{
        $details["side"]="sell";
      }
    }
  } catch (\Throwable $e) { 
    var_dump($e->getMessage());
  }
  if(is_null($details)){
    $details = array();
  }
  return $details;
}
function positionList(){
  $auth = new Auth($_ENV["key"], $_ENV["secret"], $_ENV["pass"], Auth::API_KEY_VERSION_V2);
  $api = new Position($auth);
  $list = $api->getList();
  return $list;
}
