<?php
error_reporting(E_ALL);
require __DIR__ . '/vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();
use KuCoin\Futures\SDK\PublicApi\Symbol;

require('orderFunctions.php');



// $symbol= "ONTUSDTM";

// orderProfitLossCancel($symbol);
// die();

// $symbolClass = new Symbol();
// $ticker = $symbolClass->getTicker($symbol);
// createOrder($ticker,$symbol,"sell");
// orderProfitLoss($ticker,$symbol,"buy",true);

// createOrder($ticker,$symbol,"sell");
// orderProfitLoss($ticker,$symbol,"buy",true);

// die;

$service_port = 10000;
$address = gethostbyname('bot.local');
$socket = socket_create(AF_INET, SOCK_STREAM, SOL_TCP);
if ($socket === false) {
    echo "socket_create() failed: reason: " . socket_strerror(socket_last_error()) . "\n";
}
$result = socket_connect($socket, $address, $service_port);
if ($result === false) {
    echo "socket_connect() failed.\nReason: ($result) " . socket_strerror(socket_last_error($socket)) . "\n";
}
$out = socket_read($socket, 2048);
// $out = "buy-COMPUSDTM#";

while ($out = socket_read($socket, 2048)) {
    /** read result fron server */
    $items = explode("#",$out);
    array_pop($items);
    foreach($items as $item ){
        $itemArray  =   explode("-",$item);
        $type       =   $itemArray[0];
        $symbol     =   $itemArray[1];
        // var_dump($item);
        $closeingSide = "sell";
        $side=$type;
        if($type=="sell"){
            $closeingSide="buy";
        }
        
        try {
            
            // $details = positionDetails($symbol);
            $details= array();
            $list = positionList();
            foreach($list as $item){
                if($item["symbol"]===$symbol){
                    $details=$item;
                    if(isset($details["avgEntryPrice"])){
                        if($details["avgEntryPrice"]>$details["liquidationPrice"]){
                            $details["side"]="buy";
                        }else{
                            $details["side"]="sell";
                        }
                    }
                    break;
                }
            }
            $symbolClass = new Symbol();
            $ticker = $symbolClass->getTicker($symbol);
            if($side=="close"){
                createOrder($ticker,$symbol,"buy",true);
            }else{
                if(isset($details["side"]) && $details["side"]!=$side){
                    $detailsSide = $details["side"];
                    /**closeing */
                    createOrder($ticker,$symbol,$closeingSide,true);
                }
                /**opening */
                $lossSide = ($side=="buy")? "sell" : "buy";
    
                //create order
                createOrder($ticker,$symbol,$side);
                usleep(300);
                //remove opened stop less orders
                orderProfitLossCancel($symbol);
                usleep(300);
                //create stop less
                orderProfitLoss($ticker,$symbol,$lossSide,true);
            }
        } catch (\Throwable $e) {}
    }    
}

?>