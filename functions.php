<?php

// require "predis/autoloader.php";
// phpinfo();
ini_set('display_errors', 1);
require __DIR__ . '/vendor/autoload.php';
Predis\Autoloader::register();
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();
// use KuCoin\Futures\SDK\PublicApi\Symbol;
use KuCoin\SDK\Exceptions\HttpException;
use KuCoin\SDK\Exceptions\BusinessException;
// use KuCoin\Futures\SDK\PublicApi\Symbol;
use KuCoin\SDK\PublicApi\Symbol;
use KuCoin\SDK\PublicApi\Currency;
use KuCoin\SDK\PublicApi\Time;
// use KuCoin\Futures\SDK\Http\SwooleHttp;



function getKlines(){
  // $redis = new Predis\Client(array(
  //   "scheme" => "tcp",
  //   "host" => "127.0.0.1",
  //   "port" => 6379));
  $redis = new Predis\Client();

  $period = 20;
  $time= 5; //minute
  $validCurrency=array(
    "COMPUSDTM"=>"COMP-USDT",
    "DASHUSDTM"=>"DASH-USDT",
    "ETCUSDTM"=>"ETC-USDT",
    "GRTUSDTM"=>"GRT-USDT",
    "HBARUSDTM"=>"HBAR-USDT",
    "MATICUSDTM"=>"MATIC-USDT",
    "ONTUSDTM"=>"ONT-USDT",
    "SXPUSDTM"=>"SXP-USDT",
    "XBTUSDTM"=>"BTC-USDT",
  );


  $timeClass = new Time();
  $timestamp = ceil($timeClass->timestamp()/1000);
  $localTimestamp=time();
  $localTime = date("Y-m-d H:i:s");
  // $minute = (int) date('i', $timestamp);
  // if($minute%5 != 0 ){
  //   return;
  // }

  $symbolClass = new Symbol();
  // $currency = new Currency();

  $result=array();
  foreach ($validCurrency as $symbol=>$spotSymbol) {
    try {
      // $klines = $symbolClass->getKLines($symbol,($timestamp-($period*60))*1000,$timestamp*1000,1);//for futures klines

      $hourlyklines = $symbolClass->getKLines($spotSymbol,$timestamp-(3700),$timestamp,"1hour");//for spot klines
      $hourlyOpen = $hourlyklines[0][1];
      $hourlyClose = $hourlyklines[0][2];
      $hourlySide=($hourlyOpen < $hourlyClose) ? "buy" : "sell";

      $klines = $symbolClass->getKLines($spotSymbol,($timestamp-(($period+1)*60*$time)),$timestamp,$time."min");//for spot klines
      array_shift($klines);
      $closeArr= array();
      foreach ($klines as $item) {
        $closeArr[]=$item[2];
      }
      $ema1 = ema($closeArr , $period);
      $ema2 = ema($ema1 , $period);
      $ema3 = ema($ema2 , $period);
      $out = 3 * ($ema1[0] - $ema2[0]) + $ema3[0];
  
      $open = $klines[0][1];
      $close = $klines[0][2];
  
      $openِYesterday = $klines[1][1];
      $closeYesterday = $klines[1][2];
  
      $isPositive = ($close > $open) ? true : false;
      $isNegative = ($close < $open) ? true : false;
      $isEqual = ($close == $open) ? true : false;

      
      var_dump("$spotSymbol:::$out --- $close\r\n");
      $redisValue = $redis->get($symbol);
      // if(($isPositive || $isEqual) && $close > $out && $redisValue!=="buy"){
      if($close > $out && $redisValue!=="buy"){
        if($hourlySide=="buy"){
          /** long signal */
          $result[]= "buy-$symbol";
          $redis->set($symbol,"buy");
        }else{
          $result[]= "close-$symbol";
          $redis->set($symbol,"buy");
        }
        //log
        wlog("localTime:$localTime|kucoinTimestamp:$timestamp|localTimestamp:$localTimestamp|side:buy|hourlySide:$hourlySide|symbol:$symbol|close:$close|ema:$out");
      }
      // if((!$isNegative || $isEqual )&& $close < $out && $redisValue!=="sell"){
      if($close < $out && $redisValue!=="sell"){
        if($hourlySide=="sell"){
          /** short signal */
          $result[]= "sell-$symbol";
          $redis->set($symbol,"sell");
        }else{
          $result[]= "close-$symbol";
          $redis->set($symbol,"buy");
        }
        //log
        wlog("localTime:$localTime|kucoinTimestamp:$timestamp|localTimestamp:$localTimestamp|side:sell|hourlySide:$hourlySide|symbol:$symbol|close:$close|ema:$out");
      }
    } catch (HttpException $e) {
      var_dump("kline error");
        // var_dump($e->getMessage());
    } catch (BusinessException $e) {
        // var_dump($e->getMessage());
    }
    
    usleep(333);
  }
  var_dump($result);
  return $result;
}




function ema(array $numbers, int $n){
    $numbers=array_reverse($numbers);
    $m   = count($numbers);
    $α   = 2 / ($n + 1);
    $EMA = [];

    // Start off by seeding with the first data point
    $EMA[] = $numbers[0];

    // Each day after: EMAtoday = α⋅xtoday + (1-α)EMAyesterday
    for ($i = 1; $i < $m; $i++) {
        $EMA[] = ($α * $numbers[$i]) + ((1 - $α) * $EMA[$i - 1]);
    }
    $EMA=array_reverse($EMA);
    return $EMA;
}

function wlog($log_msg){
    $log_filename = "log";
    if (!file_exists($log_filename)) 
    {
        // create directory/folder uploads.
        mkdir($log_filename, 0777, true);
    }
    $log_file_data = __DIR__."/".$log_filename.'/log_' . date('d-M-Y') . '.log';
    // if you don't add `FILE_APPEND`, the file will be erased each time you add a log
    file_put_contents($log_file_data, $log_msg . "\n", FILE_APPEND);
} 











