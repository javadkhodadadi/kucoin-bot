<?php
ini_set('display_errors', 1);
error_reporting(E_ALL);
require __DIR__ . '/vendor/autoload.php';
$dotenv = Dotenv\Dotenv::createImmutable(__DIR__);
$dotenv->load();

// require('func/CustomOrder.php');

// $customOrder = new CustomOrder();
// var_dump($customOrder->createProfitLoss());
// // createOrder("DASHUSDTM","buy");
// die;

use KuCoin\Futures\SDK\Auth;
use KuCoin\SDK\Exceptions\HttpException;
use KuCoin\SDK\Exceptions\BusinessException;
use KuCoin\Futures\SDK\PrivateApi\Order;

$auth = new Auth($_ENV["key"], $_ENV["secret"], $_ENV["pass"], Auth::API_KEY_VERSION_V2);
$order = new Order($auth);
$params = [
    "upPrice"=> 1.823,
    "downPrice"=> 1.403,
    "upStopPriceType"=> "TP",
    "downStopPriceType"=> "TP",
    "symbol"=> "SXPUSDTM"
];
$res=$order->profitLoss($params);
var_dump($res);
?>