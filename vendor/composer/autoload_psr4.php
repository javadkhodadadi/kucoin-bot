<?php

// autoload_psr4.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Workerman\\' => array($vendorDir . '/workerman/workerman'),
    'Symfony\\Polyfill\\Php80\\' => array($vendorDir . '/symfony/polyfill-php80'),
    'Symfony\\Polyfill\\Mbstring\\' => array($vendorDir . '/symfony/polyfill-mbstring'),
    'Symfony\\Polyfill\\Ctype\\' => array($vendorDir . '/symfony/polyfill-ctype'),
    'React\\Stream\\' => array($vendorDir . '/react/stream/src'),
    'React\\Socket\\' => array($vendorDir . '/react/socket/src'),
    'React\\Promise\\Timer\\' => array($vendorDir . '/react/promise-timer/src'),
    'React\\Promise\\' => array($vendorDir . '/react/promise/src'),
    'React\\EventLoop\\' => array($vendorDir . '/react/event-loop/src'),
    'React\\Dns\\' => array($vendorDir . '/react/dns/src'),
    'React\\Cache\\' => array($vendorDir . '/react/cache/src'),
    'Ratchet\\RFC6455\\' => array($vendorDir . '/ratchet/rfc6455/src'),
    'Ratchet\\Client\\' => array($vendorDir . '/ratchet/pawl/src'),
    'Psr\\Log\\' => array($vendorDir . '/psr/log/Psr/Log'),
    'Psr\\Http\\Message\\' => array($vendorDir . '/psr/http-factory/src', $vendorDir . '/psr/http-message/src'),
    'Psr\\Http\\Client\\' => array($vendorDir . '/psr/http-client/src'),
    'Predis\\' => array($vendorDir . '/predis/predis/src'),
    'PhpOption\\' => array($vendorDir . '/phpoption/phpoption/src/PhpOption'),
    'PHPSocketIO\\' => array($vendorDir . '/workerman/phpsocket.io/src'),
    'Monolog\\' => array($vendorDir . '/monolog/monolog/src/Monolog'),
    'LibDNS\\' => array($vendorDir . '/daverandom/libdns/src'),
    'League\\Uri\\' => array($vendorDir . '/league/uri/src', $vendorDir . '/league/uri-interfaces/src', $vendorDir . '/league/uri-parser/src'),
    'KuCoin\\SDK\\' => array($vendorDir . '/kucoin/kucoin-php-sdk/src'),
    'KuCoin\\Futures\\SDK\\' => array($vendorDir . '/kucoin/kucoin-futures-php-sdk/src'),
    'Kelunik\\Certificate\\' => array($vendorDir . '/kelunik/certificate/lib'),
    'GuzzleHttp\\Psr7\\' => array($vendorDir . '/guzzlehttp/psr7/src'),
    'GuzzleHttp\\Promise\\' => array($vendorDir . '/guzzlehttp/promises/src'),
    'GuzzleHttp\\' => array($vendorDir . '/guzzlehttp/guzzle/src'),
    'GrahamCampbell\\ResultType\\' => array($vendorDir . '/graham-campbell/result-type/src'),
    'Dotenv\\' => array($vendorDir . '/vlucas/phpdotenv/src'),
    'Channel\\' => array($vendorDir . '/workerman/channel/src'),
    'Amp\\WindowsRegistry\\' => array($vendorDir . '/amphp/windows-registry/lib'),
    'Amp\\Sync\\' => array($vendorDir . '/amphp/sync/src'),
    'Amp\\Socket\\' => array($vendorDir . '/amphp/socket/src'),
    'Amp\\Serialization\\' => array($vendorDir . '/amphp/serialization/src'),
    'Amp\\Process\\' => array($vendorDir . '/amphp/process/lib'),
    'Amp\\Parser\\' => array($vendorDir . '/amphp/parser/lib'),
    'Amp\\Http\\Server\\' => array($vendorDir . '/amphp/http-server/src'),
    'Amp\\Http\\Client\\' => array($vendorDir . '/amphp/http-client/src'),
    'Amp\\Http\\' => array($vendorDir . '/amphp/hpack/src', $vendorDir . '/amphp/http/src'),
    'Amp\\Dns\\' => array($vendorDir . '/amphp/dns/lib'),
    'Amp\\Cache\\' => array($vendorDir . '/amphp/cache/lib'),
    'Amp\\ByteStream\\' => array($vendorDir . '/amphp/byte-stream/lib'),
    'Amp\\' => array($vendorDir . '/amphp/amp/lib'),
);
