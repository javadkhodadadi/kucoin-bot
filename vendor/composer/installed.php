<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => 'javad/bot',
  ),
  'versions' => 
  array (
    'amphp/amp' => 
    array (
      'pretty_version' => 'v2.6.1',
      'version' => '2.6.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c5fc66a78ee38d7ac9195a37bacaf940eb3f65ae',
    ),
    'amphp/byte-stream' => 
    array (
      'pretty_version' => 'v1.8.1',
      'version' => '1.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'acbd8002b3536485c997c4e019206b3f10ca15bd',
    ),
    'amphp/cache' => 
    array (
      'pretty_version' => 'v1.5.0',
      'version' => '1.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2b6b5dbb70e54cc914df9952ba7c012bc4cbcd28',
    ),
    'amphp/dns' => 
    array (
      'pretty_version' => 'v1.2.3',
      'version' => '1.2.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '852292532294d7972c729a96b49756d781f7c59d',
    ),
    'amphp/hpack' => 
    array (
      'pretty_version' => 'v3.1.1',
      'version' => '3.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'cf4f1663e9fd58f60258c06177098655ca6377a5',
    ),
    'amphp/http' => 
    array (
      'pretty_version' => 'v1.6.3',
      'version' => '1.6.3.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e2b75561011a9596e4574cc867e07a706d56394b',
    ),
    'amphp/http-client' => 
    array (
      'pretty_version' => 'v4.6.2',
      'version' => '4.6.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '453f84f623e409889168351e60246f5feddc1b3c',
    ),
    'amphp/http-server' => 
    array (
      'pretty_version' => 'v2.1.4',
      'version' => '2.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '681b35866e6f9b90072b0202d331618915391df8',
    ),
    'amphp/parser' => 
    array (
      'pretty_version' => 'v1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f83e68f03d5b8e8e0365b8792985a7f341c57ae1',
    ),
    'amphp/process' => 
    array (
      'pretty_version' => 'v1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '3d36327bf9b4c158cf3010f8f65c00854ec3a8b7',
    ),
    'amphp/serialization' => 
    array (
      'pretty_version' => 'v1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '693e77b2fb0b266c3c7d622317f881de44ae94a1',
    ),
    'amphp/socket' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a8af9f5d0a66c5fe9567da45a51509e592788fe6',
    ),
    'amphp/sync' => 
    array (
      'pretty_version' => 'v1.4.2',
      'version' => '1.4.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '85ab06764f4f36d63b1356b466df6111cf4b89cf',
    ),
    'amphp/windows-registry' => 
    array (
      'pretty_version' => 'v0.3.3',
      'version' => '0.3.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f56438b9197e224325e88f305346f0221df1f71',
    ),
    'cash/lrucache' => 
    array (
      'pretty_version' => '1.0.0',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4fa4c6834cec59690b43526c4da41d6153026289',
    ),
    'daverandom/libdns' => 
    array (
      'pretty_version' => 'v2.0.2',
      'version' => '2.0.2.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e8b6d6593d18ac3a6a14666d8a68a4703b2e05f9',
    ),
    'evenement/evenement' => 
    array (
      'pretty_version' => 'v3.0.1',
      'version' => '3.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '531bfb9d15f8aa57454f5f0285b18bec903b8fb7',
    ),
    'graham-campbell/result-type' => 
    array (
      'pretty_version' => 'v1.0.4',
      'version' => '1.0.4.0',
      'aliases' => 
      array (
      ),
      'reference' => '0690bde05318336c7221785f2a932467f98b64ca',
    ),
    'guzzlehttp/guzzle' => 
    array (
      'pretty_version' => '7.4.1',
      'version' => '7.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'ee0a041b1760e6a53d2a39c8c34115adc2af2c79',
    ),
    'guzzlehttp/promises' => 
    array (
      'pretty_version' => '1.5.1',
      'version' => '1.5.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fe752aedc9fd8fcca3fe7ad05d419d32998a06da',
    ),
    'guzzlehttp/psr7' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '089edd38f5b8abba6cb01567c2a8aaa47cec4c72',
    ),
    'javad/bot' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'kelunik/certificate' => 
    array (
      'pretty_version' => 'v1.1.2',
      'version' => '1.1.2.0',
      'aliases' => 
      array (
      ),
      'reference' => '56542e62d51533d04d0a9713261fea546bff80f6',
    ),
    'kucoin/kucoin-futures-php-sdk' => 
    array (
      'pretty_version' => 'v1.0.18',
      'version' => '1.0.18.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c677f3d6b2022401b1f81f793d58132fb3d1f097',
    ),
    'kucoin/kucoin-php-sdk' => 
    array (
      'pretty_version' => 'v1.1.24',
      'version' => '1.1.24.0',
      'aliases' => 
      array (
      ),
      'reference' => 'a028178d09fc7ac6c3f25f8ce1dffff836f7feb1',
    ),
    'league/uri' => 
    array (
      'pretty_version' => '6.5.0',
      'version' => '6.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c68ca445abb04817d740ddd6d0b3551826ef0c5a',
    ),
    'league/uri-interfaces' => 
    array (
      'pretty_version' => '2.3.0',
      'version' => '2.3.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '00e7e2943f76d8cb50c7dfdc2f6dee356e15e383',
    ),
    'league/uri-parser' => 
    array (
      'pretty_version' => '1.4.1',
      'version' => '1.4.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '671548427e4c932352d9b9279fdfa345bf63fa00',
    ),
    'monolog/monolog' => 
    array (
      'pretty_version' => '2.3.5',
      'version' => '2.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => 'fd4380d6fc37626e2f799f29d91195040137eba9',
    ),
    'phpoption/phpoption' => 
    array (
      'pretty_version' => '1.8.1',
      'version' => '1.8.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'eab7a0df01fe2344d172bff4cd6dbd3f8b84ad15',
    ),
    'predis/predis' => 
    array (
      'pretty_version' => 'v1.1.9',
      'version' => '1.1.9.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c50c3393bb9f47fa012d0cdfb727a266b0818259',
    ),
    'psr/http-client' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '2dfb5f6c5eff0e91e20e913f8c5452ed95b86621',
    ),
    'psr/http-client-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-factory' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '12ac7fcd07e5b077433f5f2bee95b3a771bf61be',
    ),
    'psr/http-factory-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/http-message' => 
    array (
      'pretty_version' => '1.0.1',
      'version' => '1.0.1.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f6561bf28d520154e4b0ec72be95418abe6d9363',
    ),
    'psr/http-message-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0',
      ),
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.4',
      'version' => '1.1.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd49695b909c3b7628b6289db5479a1c204601f11',
    ),
    'psr/log-implementation' => 
    array (
      'provided' => 
      array (
        0 => '1.0.0 || 2.0.0 || 3.0.0',
      ),
    ),
    'ralouphie/getallheaders' => 
    array (
      'pretty_version' => '3.0.3',
      'version' => '3.0.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '120b605dfeb996808c31b6477290a714d356e822',
    ),
    'ratchet/pawl' => 
    array (
      'pretty_version' => 'v0.3.5',
      'version' => '0.3.5.0',
      'aliases' => 
      array (
      ),
      'reference' => '89ec703c76dc893484a2a0ed44b48a37d445abd5',
    ),
    'ratchet/rfc6455' => 
    array (
      'pretty_version' => 'v0.3.1',
      'version' => '0.3.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '7c964514e93456a52a99a20fcfa0de242a43ccdb',
    ),
    'react/cache' => 
    array (
      'pretty_version' => 'v1.1.1',
      'version' => '1.1.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '4bf736a2cccec7298bdf745db77585966fc2ca7e',
    ),
    'react/dns' => 
    array (
      'pretty_version' => 'v1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '2a5a74ab751e53863b45fb87e1d3913884f88248',
    ),
    'react/event-loop' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'be6dee480fc4692cec0504e65eb486e3be1aa6f2',
    ),
    'react/promise' => 
    array (
      'pretty_version' => 'v2.8.0',
      'version' => '2.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f3cff96a19736714524ca0dd1d4130de73dbbbc4',
    ),
    'react/promise-timer' => 
    array (
      'pretty_version' => 'v1.8.0',
      'version' => '1.8.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '0bbbcc79589e5bfdddba68a287f1cb805581a479',
    ),
    'react/socket' => 
    array (
      'pretty_version' => 'v1.10.0',
      'version' => '1.10.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd132fde589ea97f4165f2d94b5296499eac125ec',
    ),
    'react/stream' => 
    array (
      'pretty_version' => 'v1.2.0',
      'version' => '1.2.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '7a423506ee1903e89f1e08ec5f0ed430ff784ae9',
    ),
    'symfony/deprecation-contracts' => 
    array (
      'pretty_version' => 'v2.5.0',
      'version' => '2.5.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '6f981ee24cf69ee7ce9736146d1c57c2780598a8',
    ),
    'symfony/polyfill-ctype' => 
    array (
      'pretty_version' => 'v1.23.0',
      'version' => '1.23.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '46cd95797e9df938fdd2b03693b5fca5e64b01ce',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '9174a3d80210dca8daa7f31fec659150bbeabfc6',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.23.1',
      'version' => '1.23.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '1100343ed1a92e3a38f9ae122fc0eb21602547be',
    ),
    'vlucas/phpdotenv' => 
    array (
      'pretty_version' => 'v5.4.0',
      'version' => '5.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd4394d044ed69a8f244f3445bcedf8a0d7fe2403',
    ),
    'workerman/channel' => 
    array (
      'pretty_version' => 'v1.1.0',
      'version' => '1.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '3df772d0d20d4cebfcfd621c33d1a1ab732db523',
    ),
    'workerman/phpsocket.io' => 
    array (
      'pretty_version' => 'v1.1.12',
      'version' => '1.1.12.0',
      'aliases' => 
      array (
      ),
      'reference' => '7c4fd8738344a2950897ea97532312e791b099b7',
    ),
    'workerman/workerman' => 
    array (
      'pretty_version' => 'v4.0.25',
      'version' => '4.0.25.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd6af94dc565876ee5be30ba21b778dfee9759267',
    ),
  ),
);
